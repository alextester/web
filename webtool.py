#!/usr/bin/env python3
import os
from flask import Flask, flash, request, redirect, render_template, flash, session, abort, url_for, g
from werkzeug.utils import secure_filename
import os.path
from flask_autoindex import AutoIndex
import flask_login
from flask_login import LoginManager
from dotenv import load_dotenv
import sqlite3
from FDataBase import FDataBase
from flask_sqlalchemy import SQLAlchemy
from flask import Flask


load_dotenv() #Для загрузки дополнительной переменной окружения из файла .env не имзеняя основную


DATABASE = '/tmp/'+os.environ['simple']
DEBUG = True
SECRET_KEY = 'fdgfh78@#5?>gfhf89dx,v06k'
USERNAME = 'admin'
PASSWORD = 'qwerty'


user = os.environ['userweb']          # Переменная для пользователя, admin по умолчанию
password = os.environ['passwordweb']      # Переменная для пароль, admin по умолчанию
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['database']
app.config.from_object(__name__)
app.config.update(dict(DATABASE=os.path.join(app.root_path, os.environ['simple'])))
app.config['SECRET_KEY'] = 'gjhgjhgjg65jgjhfjfjkjhjkhjbkb'      # Произвольный ключ
# LoginManager = LoginManager(app)
menu = [            # Основное меню страницы
        {"name": "Регистрация", "url": "register"},
        {"name": "Вход", "url": "test"},
        {"name": "Выход", "url": "logout"},
        {"name": "Загрузка файлов", "url": "upload"},
        {"name": "Просмотр файлов", "url": "files"},
        {"name": "О сайте", "url": "about"},
        {"name": "Добавить отзыв", "url": "add_post"},
        {"name": "Отзывы", "url": "posts"},
        {"name": "Postgres", "url": "sql"}
                                                      ]

db=SQLAlchemy(app)



class userbase(db.Model):
  __tablename__='users'
  id=db.Column(db.Integer,primary_key=True)
  username=db.Column(db.String(40))
  email=db.Column(db.String(40))
  role=db.Column(db.String(40))

  def __init__(self, username, email, role):
    self.username=username
    self.email=email
    self.role=role


@app.route('/sql')
def sql():
  return render_template('index2.html', menu=menu)

@app.route('/submit', methods=['POST'])
def submit():
  username = request.form['username']
  email = request.form['email']
  role=request.form['role']

  users=userbase(username, email, role)
  db.session.add(users)
  db.session.commit()


  usersResult=db.session.query(userbase).filter(users.id==1)
  for result in usersResult:
    print(result.username)
  flash('Успешная регистрация', category='success')
  return render_template('index2.html', menu=menu)


def connect_simpledb():
    conn = sqlite3.connect(app.config['DATABASE'])
    conn.row_factory = sqlite3.Row
    return conn


def create_simpledb():
    simpledb = connect_simpledb()
    with app.open_resource('sq_db.sql', mode='r') as f:
        simpledb.cursor().executescript(f.read())
    simpledb.commit()
    simpledb.close()


def get_simpledb():
    if not hasattr(g, 'link_db'):
        g.link_simpledb = connect_simpledb()
    return g.link_simpledb


@app.route("/add_post", methods=["POST", "GET"])
def addPost():
    simpledb = get_simpledb()
    simpledbase = FDataBase(simpledb)

    if request.method == "POST":
        if len(request.form['name']) > 4 and len(request.form['post']) > 10:
            res = simpledbase.addPost(request.form['name'], request.form['post'])
            if not res:
                flash('Неверный формат сообщения или иная ошибка', category='error')
            else:
                flash('Ваше сообщение отправлено', category='success')
        else:
            flash('Неверный формат сообщения или иная ошибка', category='error')

    return render_template('add_post.html', menu=menu, title="Добавление отзыва")


@app.teardown_appcontext
def close_simpledb(error):
    if hasattr(g, 'link_db'):
        g.link_simpledb.close()


@app.route("/post/<int:id_post>")
def showPost(id_post):
    if 'userLogged' not in session or session['userLogged'] != user:
        abort(401)

    simpledb = get_simpledb()
    simpledbase = FDataBase(simpledb)
    title, post = simpledbase.getPost(id_post)
    if not title:
        abort(404)

    return render_template('post.html', menu=menu, title=title, post=post)


@app.route("/")
def index():
    return render_template('index.html', menu=menu)


@app.route("/posts")
def posts():
    if 'userLogged' not in session or session['userLogged'] != user:
        abort(401)

    simpledb = get_simpledb()
    simpledbase = FDataBase(simpledb)

    return render_template('index3.html', menu=menu, posts=simpledbase.getPostsAnonce())


@app.route("/about")
def about():

    return render_template('about.html', title="О сайте", menu=menu)


@app.route("/profile/<username>")       # Проверка сессии пользователя
def profile(username):
    if 'userLogged' not in session or session['userLogged'] != username:
        abort(401)

    return redirect(url_for('upload_file', username=session['userLogged']))


@app.route('/logout')
def logout():
    global user, password
    session.pop('username', None)
    user = ''
    password = ''
    return redirect(url_for('index'))


@app.route("/register", methods=["POST", "GET"])            # регистрация пользователя
def register():
    global user, password
    if request.method == "POST":
        user = request.form['username']
        password = request.form['psw']
        flash('Успешная регистрация', category='success')

    return render_template('register.html', title="Регистрация", menu=menu)


@app.route("/test", methods=["POST", "GET"])        # Вход пользователя после регистрации
def login():
    global user, password
    if 'userLogged' in session:
        return redirect(url_for('profile', username=session['userLogged']))
    if request.method == "POST":
        if request.form['username'] == user and request.form['psw'] == password:
            session['userLogged'] = request.form['username']
            return render_template('upload.html', title='Загрузка файла', menu=menu)
    return render_template('test.html', title="Авторизация", menu=menu)


@app.errorhandler(404)              # Страница ошибки
def pageNotFount(error):
    return render_template('page404.html', title="Страница не найдена", menu=menu), 404


with app.test_request_context():
    print(url_for('index'))


folder = './files'                          # загрузка файлов
app.config['UPLOAD_FOLDER'] = folder


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if 'userLogged' not in session or session['userLogged'] != user:
        abort(401)
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('Пусто', category='error')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('Не выбран файл', category='error')
            return redirect(request.url)
        if file:
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('upload_file', filename=filename))

    return render_template('upload.html', title="Загрузка файла", menu=menu)


if os.environ['pathweb'] == "disable":
    files_index = AutoIndex(app, os.environ['pluspath'], add_url_rules=False)      # просмотр файлов
else:
    files_index = AutoIndex(app, eval(os.environ['pathweb'])+os.environ['pluspath'], add_url_rules=False)


@app.route('/files')
@app.route('/files/<path:path>')
def autoindex(path='.'):
    if 'userLogged' not in session or session['userLogged'] != user:
        abort(401)

    return files_index.render_autoindex(path)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', ssl_context=('cert.pem', 'key.pem'))
